const webpack = require('webpack');

const getModuleData = require('./utils/module-package');

module.exports = ({ prod }) => {
    process.env.NODE_ENV = prod ? 'production' : 'development';
    const moduleData = getModuleData();
    process.env.ENTRY = moduleData.entryPoint;
    
    const config = require('@prepos/webpack-config');
    config.output.publicPath = `/static/${moduleData.cleanName}/1.0.0/`
    
    webpack(config, (err, stats) => {
        if (err) {
          console.error(err);
          return;
        }

        console.log(stats.toString({
          chunks: false,  // Makes the build much quieter
          colors: true    // Shows colors in the console
        }));
    });
}