const fs = require('fs');

const init = () => {
    const dependencies = [
        'react',
        'react-dom',
        'typescript',
    ]
    const devDeps = [
        '@types/react',
        '@types/react-dom'
    ]
    require('child_process').execSync(`npm install --save ${dependencies.join(' ')}`, { stdio: 'inherit' });
    require('child_process').execSync(`npm install --save-dev ${devDeps.join(' ')}`, { stdio: 'inherit' });

    const path = require('path');
    const packagepath = path.resolve('package.json');

    const package = require(packagepath);
    package.scripts = package.scripts || {};
    package.scripts.start = 'prepos-cli --server';

    const tsConfigPath = path.resolve('tsconfig.json');
    const preposConfig = path.resolve('prepos.config.js');

    if (!fs.existsSync(tsConfigPath)) {
        const tsConfig = {
            compilerOptions: {
                target: 'es6',
                module: 'esnext',
                lib: ['DOM', 'ESNEXT'],
                jsx: 'react',
                strict: true,
                noImplicitAny: false,
                esModuleInterop: true,
                skipLibCheck: true,
                forceConsistentCasingInFileNames: true,
                moduleResolution: 'node',
            },
            exclude: [
              'node_modules',
              '**/*.test.ts',
              '**/*.test.tsx',
              'node_modules/@types/jest'
            ]
        }

        fs.writeFileSync(tsConfigPath, JSON.stringify(tsConfig, null, 4));
    }

    if (!fs.existsSync(preposConfig)) {
        const config = `module.exports = {
    webpackConfig: {},
    config: {},
    navigation: {},
    features: {
        'dummy.fature1': true,
    },
}`

        fs.writeFileSync(preposConfig, config);
    }

    fs.writeFileSync(packagepath, JSON.stringify(package, null, 4));
}

module.exports = init;
